import mysql.connector


# -----------------------------------------------------------
# @ description creates a connection and cursor object after connected the database.
# Then creates all necessary elements to insert data in database, and also
# displays user provided query's result.
# @ author Cuneyt Yildirim 2037793
# @ self.__user : database's user
# @ self.__host : database's hostname
# @ self.__passwd : database's password
# @ self.__db_name : database's name
# @ self.__db_obj : connectivity object
# @ self.__db_cursor: connectivity cursor

# -----------------------------------------------------------


class DatabaseApi:
    """init module that initialize and creates a connectivity with database"""

    def __init__(self, host, user, passwd, db_name):
        self.__user = user
        self.__host = host
        self.__passwd = passwd
        self.__db_name = db_name
        self.__db_cursor = None
        self.__db_obj = self.__connect_db()

    def __connect_db(self):
        """Creates a connection with database and set the cursor object"""
        try:
            if self.__db_name is not None:  # Checks database name is exist or not
                tmp_con = mysql.connector.connect(host=self.__host, user=self.__user, passwd=self.__passwd,
                                                  database=self.__db_name)
                self.__db_cursor = tmp_con.cursor()
                print(f'Get connected database with provided database\'s name {self.__db_name} ')
                return tmp_con
            else:
                # If the database name does not exist connect database with basic credential
                # and creates a cursor and then create a database
                tmp_con = mysql.connector.connect(host=self.__host, user=self.__user, passwd=self.__passwd)
                self.__db_cursor = tmp_con.cursor()
                print("Get connected without database's name")
                self.__create_database()
                return tmp_con
        except mysql.connector.Error as error:  # If any error occurs during the connection
            print('Some problem with database : {}'.format(error))

    def __create_database(self):
        """Creating the database with specific name"""
        self.__db_name = "covid_corona_db_yild_hoan"
        self.__db_cursor.execute(f'create database if not exists {self.__db_name}')
        # print(f'Database {self.__db_name} created')
        self.__select_database()
        # print(f"Database {self.__db_name} is selected.")

    def __select_database(self):
        """Selecting Database to use it"""
        use_query = f'use {self.__db_name}'
        self.__db_cursor.execute(use_query)

    def create_table(self, table_name, table_schema):
        """Creates tables with necessary parameters"""
        create_table_query = f"Create table {table_name} ({table_schema});"

        # Executes the query
        self.__db_cursor.execute(create_table_query)
        self.__db_obj.commit()
        # print(f"Created database's {table_name} table.")

    def populate_data(self, table_name, record_list):
        """Inserting data to a database"""
        # for element in record_list:
        parameter_data = '%s,' * len(record_list[0])
        parameter_data = parameter_data[:-1]
        query = f'insert into {table_name} values ({parameter_data})'

        self.__db_cursor.executemany(query, record_list)
        self.__db_obj.commit()
        # print(f'Data entering is done in the {table_name}')

    def drop_table(self):
        """Dropping tables"""
        self.__db_cursor.execute("drop table if exists country_neighbour_table")
        self.__db_cursor.execute("drop table if exists Neighbour_table")
        self.__db_cursor.execute("drop table if exists Country_table")
        self.__db_cursor.execute("drop table if exists Covid_data_table")
        # print("Tables are dropped")

    def execute_query(self, query, val):
        """Executes a query with given parameters"""
        self.__db_cursor.execute(query, [val])
        return self.__db_cursor.fetchall()

    def close_database(self):
        print("Database is closing...")
        self.__db_cursor.close()

    def exist_country_ck(self):
        """Checks name differences of the country""" \
        "select distinct CountryOther from covid_data_table where CountryOther not in (select distinct country from %s)"
        query_list = "Country_table"
        que = "select distinct CountryOther from covid_data_table  where CountryOther not in (select distinct country from Country_table)"
        self.__db_cursor.execute(que)
        test = self.__db_cursor.fetchall()
        return test

