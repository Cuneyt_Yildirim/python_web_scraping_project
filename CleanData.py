def clean_list_element(country):
    tmp2 = []
    for item in country[1:]:
        new_item = [item[0], item[1]]
        for idx in range(3, len(item)):
            if idx < 18:
                if "," in item[idx] or "+" in item[idx] or item[idx].isdigit():
                    new_item.append(int(item[idx].replace(",", "").replace("+", "")))
                elif item[idx] == '' or item[idx] == 'N/A':
                    item[idx] = None
                    new_item.append(item[idx])
                else:
                    new_item.append(item[idx])

        tmp2.append(new_item)
    return tmp2


def convert_list_dict(data_list):
    list_tmp = []
    for item in data_list:
        d = {'id': item[0], 'day': item[1], 'CountryOther': item[2], 'TotalCases': item[3],
             'NewCases': item[4], 'TotalDeaths': item[5],
             'NewDeaths': item[6], 'TotalRecovered': item[7],
             'NewRecovered': item[8], 'ActiveCases': item[9],
             'Serious,Critical': item[10], 'TotCases_1M_pop': item[11],
             'Deaths_1M_pop': item[12], 'TotalTests': item[13],
             'Tests_1M_pop': item[14], 'Population': item[15],
             'Continent': item[16]}
        list_tmp.append(d)
    return list_tmp


def cleaning_cursor(raw_data):
    cleaned_list_data = []
    if isinstance(raw_data, tuple):
        return str(raw_data)[2:-3].replace("'", "")
    elif isinstance(raw_data, list):
        if len(raw_data) > 1:
            for elem in raw_data:
                tmp = str(elem)[2:-3].replace("'", "")
                cleaned_list_data.append(tmp)
        else:
            return str(raw_data)[2:-3].replace("'", "")

        return cleaned_list_data


# def clean_dict_element(country):
#     tmp2 = []
#     for item in country[1:]:
#         tmp2.append(
#             {'day': item[0], 'Country,Other': item[1], 'TotalCases': item[2].replace(",", ""),
#              'NewCases': item[3].replace(",", ""), 'TotalDeaths': item[4].replace(",", ""),
#              'NewDeaths': item[5].replace(",", ""), 'TotalRecovered': item[6].replace(",", ""),
#              'NewRecovered': item[7].replace(",", ""), 'ActiveCases': item[8].replace(",", ""),
#              'Serious,Critical': item[9].replace(",", ""), 'TotCases/1M pop': item[10].replace(",", ""),
#              'Deaths/1M pop': item[11].replace(",", ""), 'TotalTests': item[12].replace(",", ""),
#              'Tests/1M pop': item[13].replace(",", ""), 'Population': item[14].replace(",", ""),
#              'Continent': item[15]})
#     return tmp2
