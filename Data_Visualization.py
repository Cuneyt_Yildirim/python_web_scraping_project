# -----------------------------------------------------------
# @ description
#               Visualize the data creating a bar char
# @ author Cuneyt Yildirim 2037793
# @ self.__data = this is scrapped data, and
#               after it converts a dataframe by using pandas and uses to create a charts

# -----------------------------------------------------------

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


class Data_Visualization:
    def __init__(self, data):
        self.__data = data

    def create_data_frame(self, based_country, query_data):
        pass

    def largest_data_plotting(self, based_country, query_data):
        """Comparison with the largest border country's covid19 new cases"""
        data_frame = pd.DataFrame(self.__data)
        home_df = data_frame.loc[data_frame['CountryOther'] == based_country]
        abroad_df = data_frame.loc[data_frame['CountryOther'] == query_data]
        days = list(abroad_df.day)

        home_case = list(home_df.NewCases)
        abroad_case = list(abroad_df.NewCases)

        "the ratio values"
        abroad_y_value = [r / 1000 for r in home_df.NewCases]
        home_y_value = [r / 1000 for r in abroad_df.NewCases]

        days = [str(t) for t in days]
        x_axis = np.arange(len(days))
        plt.xlabel("Dates on March")
        plt.ylabel("New Cases 1k")

        plt.title(f"6 days new cases comparison between {based_country}\n and longest border neighbours {query_data}")
        abroad_bars = plt.bar(x_axis - 0.2, abroad_case, 0.4, label="NewCases in " + query_data, color='y',
                              align='center')
        home_bars = plt.bar(x_axis + 0.2, home_case, 0.4, label="NewCases in " + based_country, color='g',
                            align='center')
        plt.xticks(x_axis, days)

        self.__add_bar_value(home_bars)   # adding bar values on top it
        self.__add_bar_value(abroad_bars)   # adding bar values on top it
        plt.legend()
        plt.savefig(f'reports_results/{based_country}_comparison_largest_border.jpg')
        plt.show()

    def three_borders_plotting(self, based_country, query_data):
        """Comparisons with the largest 3 borders neighbours countries"""
        data_frame = pd.DataFrame(self.__data)
        home_df = data_frame.loc[data_frame['CountryOther'] == based_country]
        days = list(home_df.day)
        home_df = home_df.Deaths_1M_pop

        countries_df = []
        for country in query_data:
            countries_df.append(data_frame.loc[data_frame['CountryOther'] == country].Deaths_1M_pop)

        days = [str(t) for t in days]
        x_axis = np.arange(len(days))
        plt.xlabel("Dates on March")
        plt.ylabel("Deaths/1M pop")

        plt.title(f"6 days new cases comparison between {based_country}\n and 3 neighbours "
                  f"{query_data[0], query_data[1], query_data[2]}")

        plt.bar(x_axis - 0.2, home_df, 0.2, label="Deaths/1M pop in " + based_country, color='b', align='center')
        plt.bar(x_axis + 0.2, countries_df[0], 0.2, label="Deaths/1M pop in " + query_data[0], color='r',
                align='center')
        plt.bar(x_axis + 0.4, countries_df[1], 0.2, label="Deaths/1M pop in " + query_data[1], color='c',
                align='center')
        plt.bar(x_axis + 0.6, countries_df[2], 0.2, label="Deaths/1M pop in " + query_data[2], color='g',
                align='center')
        plt.xticks(x_axis, days)
        plt.legend()
        plt.savefig(f'reports_results/{based_country}_comparison_based_3_neighbours.jpg')
        plt.show()

    def country_evolution(self, based_country):
        """Shows country evolution based on the new"""
        data_frame = pd.DataFrame(self.__data)

        home_df = data_frame.loc[data_frame['CountryOther'] == based_country]
        cases = home_df[['NewCases', 'NewRecovered', 'NewDeaths']]
        days = list(home_df.day)
        # days = [str(t) for t in days]
        x_axis = np.arange(len(days))
        plt.xlabel("Dates on March")
        plt.ylabel("3 main indicators")

        plt.title(f"6 days key indicators evolution in {based_country}")
        plt.bar(x_axis - 0.2, cases.NewCases, 0.2, label="NewCases in " + based_country, color='b', align='center')
        plt.bar(x_axis - 0.4, cases.NewRecovered, 0.2, label="NewRecovered in " + based_country, color='r',
                align='center')
        death_bars = plt.bar(x_axis - 0.6, cases.NewDeaths, 0.2, label="NewDeaths in " + based_country, color='g',
                             align='center')
        plt.xticks(x_axis, days)
        self.__add_bar_value(death_bars)  # adding bar values on top it
        plt.legend()
        plt.savefig(f'reports_results/{based_country}_evolution.jpg')
        plt.show()

    @staticmethod
    def __add_bar_value(abroad_bar):
        """Adds bar's value top of the column"""
        width = 0.35
        for bar in abroad_bar:
            height = bar.get_height()
            plt.annotate('{}'.format(height),
                         xy=(bar.get_x() + bar.get_width() / 2, height),
                         xytext=(1, 3),
                         textcoords="offset points", ha='center', va='bottom')
