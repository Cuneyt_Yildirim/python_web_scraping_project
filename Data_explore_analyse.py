# -----------------------------------------------------------
# @ description
# @ author Cuneyt Yildirim 2037793
# @ self.__database_setup : Gets file name from the user and starts to scrapping those data
#                            and return suitable data to use other modules
# @ self.__dt_plot : Object of Data_Visualization module when data is available set the data into it
# @ self.__country_name_list : The getter of the country list of the dictionary
# @ self.__general_setup() : starts automatically general execution related with itself

# -----------------------------------------------------------
from CleanData import convert_list_dict
from Data_Visualization import Data_Visualization
from Database_management import *
from ScrapeClass import ScrapeClass
import os


class Data_explore_analyse:
    def __init__(self):
        self.__database_setup = Database_management()
        self.__dt_plot = None
        self.__country_name_list = self.__database_setup.get_country_name_dict()
        self.__general_setup()

    def __general_setup(self):
<<<<<<< HEAD
        path1 = "file:///C:/Users/cuney/OneDrive%20-%20Dawson%20College/W_Python/Labs" \
                         "/python_web_scraping_project/local_html/"

        file_name1 = f"{path1}/local_page_2022_03_22.htm"
        file_name2 = f"{path1}/local_page_2022_03_25.htm"
=======
>>>>>>> 2d05e74ec941418588ba9ac853f2c14a9e0391df

        path = "https://www.worldometers.info/coronavirus/#main_table"

        print("1. Do you want to plot today's covid19 data's.\n"
              "2. Plot from the local data\n")
        choice = int(input("Select one of the option\n"))

        while choice != 1 and choice != 2:
            print("Please select only one the option\n")
        if choice == 1:
            sc_data1 = ScrapeClass(path)
            sc_data1.get_html()
            tmp_data = sc_data1.get_countries_list()
            tmp_dict = convert_list_dict(tmp_data)
            self.__dt_plot = Data_Visualization(tmp_dict)
            self.__database_setup.insert_covid_data("Covid_data_table", tmp_data)

        else:
            sc_data1 = ScrapeClass(path)
            sc_data1.look_file()
            tmp_data = sc_data1.get_countries_list()
            tmp_dict = convert_list_dict(tmp_data)
            self.__dt_plot = Data_Visualization(tmp_dict)
            self.__database_setup.insert_covid_data("Covid_data_table", tmp_data)

    def get_database_setup(self):
        return self.__database_setup

    def get_country_name_list(self):
        return self.__country_name_list

    def get_dt_plotting(self):
        return self.__dt_plot
