# -----------------------------------------------------------
# @description reads provided  txt file and returns list of string
#
# @author Cuneyt YILDIRIM
#
# -----------------------------------------------------------
import csv
import json


def read_csv(file_name) -> list:
    """Reads provided txt file with default parameters and creates a list of string"""
    file_obj = open(file_name, "rt")
    tmp_list = []
    while True:
        lines = file_obj.readline()
        if not lines:
            break
        tmp_list.append(lines)

    # closing opened file.
    file_obj.close()
    return tmp_list


def read_json(file_name) -> dict:
    """Reads json files"""
    with open(file_name) as js:
        data = json.load(js)
    return data


def save_to_file(path_to_save, filename, option):
    """Save file inside the csv file"""
    file = open(path_to_save, option, newline='')
    cols = []
    file_cols = filename[0][000]
    for i in file_cols.keys():
        cols.append(i)
    csv_fi = csv.DictWriter(file, fieldnames=cols)
    csv_fi.writeheader()
    for i in range(len(filename)):
        csv_fi.writerows(filename[i])
    file.close()
