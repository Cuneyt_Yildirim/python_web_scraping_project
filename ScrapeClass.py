# -----------------------------------------------------------
# @ description
#           Scrapping data and returns a list of countries with their covid data, date of scrapping and id's
# @ author Cuneyt Yildirim 2037793
# @ self.__url = file name for scrapping
# @ self.__id = creates an id for each row to use inserting data in database
# @ self.__binary_html_data = a binary data after read the file
# @ self.__countries = : cleaned list of countries after scrapped it

# -----------------------------------------------------------
import os
from urllib.request import urlopen, Request
from CleanData import *
from bs4 import BeautifulSoup

# path1 = "./local_html/local_page_2022_03_16.htm"
# path2 = "./local_html/local_page_2022_03_25.htm"


class ScrapeClass:
    def __init__(self, url, id=0):
        self.__url = url
        self.__id = id
        self.__binary_html_data = None
        self.__countries = []


    def get_html(self):
        req = Request(self.__url, headers={'user-agent': 'Mozilla/5.0'})
        html_response = urlopen(req)
        binary_html_data = html_response.read()
        self.__binary_html_data = binary_html_data
        self.__optimize_data()

    """Reads local html files"""
    def read_local(self, path):
        new_path = "file:"+path
        req = Request(new_path, headers={'user-agent': 'Mozilla/5.0'})
        html_response = urlopen(req)
        binary_html_data = html_response.read()
        self.__binary_html_data = binary_html_data
        self.__optimize_data()
        

    def look_file(self):
        """Looks through all files inside the local_html"""
        lists = os.listdir("local_html") 

        for file in range(len(lists)):
            day = input("Enter the day of number\n")
            file_url = "local_html/local_page2022-03-" + day + ".html"
            if os.path.isfile(file_url):
                self.read_local(file_url)
            else:
                pass

    def __get_bs4(self):
        """Parse the file an object to read by other module."""
        root_objs = BeautifulSoup(self.__binary_html_data, "html.parser")
        return root_objs

    def __optimize_data(self):
        """Scrap the file and creates a list object"""
        root_objects = self.__get_bs4()

        raw_date = root_objects.find(text=lambda date: 'Last updated' in date)
        date_today = self.extract_day(raw_date)

        today_table = root_objects.find('table', id='main_table_countries_today').find('tbody')
        yesterday_table = root_objects.find('table', id='main_table_countries_yesterday').find('tbody')
        yesterday2_table = root_objects.find('table', id='main_table_countries_yesterday2').find('tbody')

        self.__countries.extend(self.__find_table_elements(yesterday2_table, date_today-2))
        self.__countries.extend(self.__find_table_elements(yesterday_table, date_today -1))
        self.__countries.extend(self.__find_table_elements(today_table, date_today))

    def extract_day(self, raw_data):
        tmp_day = raw_data.split(',')
        day = tmp_day[0].split(" ")
        return int(day[3])

    def __find_table_elements(self, table_row, day) -> list:
        tmp = []
        rows = table_row.findAll('tr', style='')
        for row in rows:
            columns = row.find_all('td')
            country_detail = [column.text.strip() for column in columns]
            country_detail.insert(0, self.__id)
            self.__id += 1
            country_detail.insert(1, day)
            tmp.append(country_detail)
        tmp = clean_list_element(tmp)
        return tmp

    def get_countries_list(self):
        return self.__countries

    def get_bin_data(self):
        return self.__binary_html_data

    def save_html(self):
        try:
            file = input("Enter the file name")
            f = open("./local_html/local_page_"+file+".htm")
            f.write(self.__binary_html_data)
            f.close()
        except IOError as error:
            print('I/O error')
