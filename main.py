# -----------------------------------------------------------
# @description main module of the execution
#
# @author Cuneyt YILDIRIM
#
# -----------------------------------------------------------
from Data_explore_analyse import *


class Application:

    def process(self):
        visual_setup = Data_explore_analyse()
        db_setup = visual_setup.get_database_setup()
        name_list = visual_setup.get_country_name_list()

        # Plots data set and gets the plotted data
        dt_plot = visual_setup.get_dt_plotting()

        print("Those countries name's and their datas are different for both tables,"
              "so be wise selecting the name of the covid 19 data")
        visual_setup.get_database_setup().existing_country_list()

        doesExist = True
        query_exist = True

        country_name = input("Please enter a country name.\n")

        # First data validation entered data is exist in json file
        while doesExist:
            if country_name in name_list:
                print(f"You entered {country_name} as a name of country that does exist.")
                doesExist = False
            else:
                country_name = input(f"{country_name} does not exist in the our Database.\n"
                                     f"Please re-enter again country name.\n")

        # Second data validation the name of country does exist in database
        query = "select IFNULL((select distinct CountryOther from covid_data_table " \
                "where CountryOther = %s), 'country name not found in database.')"
        country_exist = db_setup.get_query_data(query, country_name)
        cleaned_country = cleaning_cursor(country_exist)
        # cleaned_country = db_setup.existing_country_list()
        doesContain = True
        while doesContain:
            if cleaned_country == country_name:
                print(f"{country_name} does exist in country list.")

                # Getting necessary information from database
                cleaned_neighbours_data = visual_setup.get_database_setup().three_neighbours(country_name)
                cleaned_border_data = visual_setup.get_database_setup().largest_border(country_name)

                # Calling plotting modules
                dt_plot.three_borders_plotting(country_name, cleaned_neighbours_data)
                dt_plot.largest_data_plotting(country_name, cleaned_border_data)
                dt_plot.country_evolution(country_name)
                doesContain = False
                pass
            else:
                print("Country name does not exist. Please Enter correctly")
                visual_setup.get_database_setup().close_database()
                break


if __name__ == '__main__':
    run_app = Application()
    run_app.process()
