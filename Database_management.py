# -----------------------------------------------------------
# @ description
#                 Manages all database relations with other modules like a bridge table.
#                 Other tables can access the database using this module
# @ author Cuneyt Yildirim 2037793
# self.__my_sql_obj : The Database_Api object
# self.__file_json : the converted version of countries.json file
#                   to the readable format by other modules and functions
# self.__country_name_list : self.__file_json array version
# self.__country_name_dict : self.__file_json dictionary version
# self.__db_setup() : General execution with related the database.
#                    The dropping, creating and inserting data into tables
#                    without inserting the covid_data_table
#                    because its data will provide the future of execution.

# -----------------------------------------------------------

from CleanData import cleaning_cursor
from User_info import *
from DatabaseApi import *
from File_IO import read_json


class Database_management:

    def __init__(self):
        self.__my_sql_obj = None
        self.__file_json = read_json('countries/country_neighbour_dist_file.json')
        self.__country_name_list = None
        self.__country_name_dict = None
        self.__db_setup()

    def __db_setup(self):
        user = User_info(None)
        db_connection = DatabaseApi(user.get_host_name(), user.get_user_name(),
                                    user.get_pass(), user.get_db_name())
        self.__my_sql_obj = db_connection
        self.__my_sql_obj.drop_table()
        self.__create_covid_data_table()
        self.__create_country_table()
        self.__create_neighbours_table()
        self.__create_neighbour_country_table()
        self.__insert_data()
        print("Database's tables and data's setup successfully.\n")

    def __create_covid_data_table(self):
        covid_schema = "id int(100) not null,\n" \
                       "data_date int(3) not null,\n" \
                       "CountryOther varchar(50) not null,\n" \
                       "TotalCases int,\n" \
                       "NewCases int,\n" \
                       "TotalDeaths int,\n" \
                       "NewDeaths int,\n" \
                       "TotalRecovered int,\n" \
                       "NewRecovered int,\n" \
                       "ActiveCases int,\n" \
                       "SeriousCritical int,\n" \
                       "TotCases1Mpop int,\n" \
                       "Deaths1Mpop int,\n" \
                       "TotalTests int,\n" \
                       "Tests1Mpop int,\n" \
                       "Population int,\n" \
                       "Continent varchar(20),\n" \
                       "PRIMARY KEY(id)"
        self.__my_sql_obj.create_table("Covid_data_table", covid_schema)

    def __create_country_table(self):
        country_schema = "countryID int(4) not null,\n" \
                         "country varchar(300),\n" \
                         "primary key(countryID)"
        self.__my_sql_obj.create_table("Country_table", country_schema)

    def __create_neighbours_table(self):
        schema = "neighbourID int(4) not null,\n" \
                 "neighbour varchar(300),\n" \
                 "primary key(neighbourID)"
        self.__my_sql_obj.create_table("Neighbour_table", schema)

    def __create_neighbour_country_table(self):
        # print("neighbour country table is created")
        schema = "countryID int(4) not null,\n" \
                 "neighbourID int(4) not null,\n" \
                 "distance decimal(7,2),\n" \
                 "CONSTRAINT cid_ref FOREIGN KEY (countryID) REFERENCES Country_table (countryID),\n" \
                 "CONSTRAINT neighid_red FOREIGN KEY (neighbourID) REFERENCES Neighbour_table (neighbourID)\n"
        self.__my_sql_obj.create_table("country_neighbour_table", schema)

    def __insert_data(self):
        """Inserting data for the country data's information"""
        county_table = self.__country_list_ver()
        self.__my_sql_obj.populate_data("country_table", county_table)

        neighbour_data = self.__neighbour_list_ver()
        self.__my_sql_obj.populate_data("neighbour_table", neighbour_data)

        neighbour_country_table = self.__create_neighbour_country_list_data()
        self.__my_sql_obj.populate_data("country_neighbour_table", neighbour_country_table)

    def __country_list_ver(self) -> list:
        """Converting country dictionary to list to"""
        tmp = []
        country_dict = self.__create_country_dict_data()
        for key, val in country_dict.items():
            tmp2 = [val, key]
            tmp.append(tmp2)
        return tmp

    def __neighbour_list_ver(self) -> list:
        """Converting neighbour dictionary to list to"""
        tmp = []
        neighbour_dict = self.__create_neighbour_dict_data()
        for key, val in neighbour_dict.items():
            tmp2 = [val, key]
            tmp.append(tmp2)
        return tmp

    def __create_country_dict_data(self) -> dict:
        """Creating the country dictionary with adding country id"""
        tmp = {}
        cid = 1
        for country in self.__file_json:
            for key, val in country.items():
                if key not in tmp:
                    tmp[key] = cid
                    cid += 1
        return tmp

    def __create_neighbour_dict_data(self) -> dict:
        """Creating the neighbour dictionary with adding neighbour id """
        tmp = {}
        bid = 1
        for element in self.__file_json:
            for country, values in element.items():
                for neighbour, distance in values.items():
                    if neighbour not in tmp:
                        tmp[neighbour] = bid
                        bid += 1
        return tmp

    def __create_neighbour_country_list_data(self) -> list:
        """Creates the bridge table's data in the database"""
        tmp = []
        country_cid = self.__create_country_dict_data()
        neighbour_bid = self.__create_neighbour_dict_data()
        for elements in self.__file_json:
            for country, values in elements.items():
                for neighbour, distance in values.items():
                    neighbour_country = [country_cid.get(country), neighbour_bid.get(neighbour), distance]
                    tmp.append(neighbour_country)
        return tmp

    def insert_covid_data(self, table_name, record_list):
        """Insertscovid_table data in itself"""
        self.__my_sql_obj.populate_data(table_name, record_list)

    def get_query_data(self, query, value):
        """Execues any query with provided parameters"""
        return self.__my_sql_obj.execute_query(query, value)

    def get_country_name_dict(self):
        return self.__create_country_dict_data()

    def three_neighbours(self, country_name) -> list:
        three_largest_borders_query = "select  neighbour from country_table " \
                                      "join country_neighbour_table using(countryID) " \
                                      "join neighbour_table using(neighbourID) " \
                                      "where country = %s " \
                                      "order by distance " \
                                      "desc limit 3;"
        three_largest_borders = self.get_query_data(three_largest_borders_query, country_name)
        return cleaning_cursor(three_largest_borders)

    def largest_border(self, country_name):
        """Gets the largest border country stored in the database.
        Used limit 1 that some country can have same distant with another country
        or some of them shows in json file 0 distance like. """
        largest_border_query = "select neighbour from neighbour_table " \
                               "join country_neighbour_table using(neighbourID) " \
                               "join country_table using (countryID) " \
                               "where country = %s " \
                               "order by distance desc " \
                               "limit 1;"

        largest_border = self.get_query_data(largest_border_query, country_name)
        return cleaning_cursor(largest_border)

    # @staticmethod
    def existing_country_list(self):
        """There is two different dataset and some country has store in it with different name.
        Thus, if user enters any name of them program can not proper result about the covid19."""

        name_differences_country_list = \
            ['China', 'USA', 'France', 'UKS', 'Korea', 'Netherlands', 'Czechia', 'UAE', 'Réunion', 'Martinique',
             'Guadeloupe', 'Angola', 'DRC', 'Ivory Coast', 'French Guiana', 'French Polynesia', 'Channel Islands',
             'New Caledonia', 'Cabo Verde', 'Curaçao', 'Mayotte', 'Faeroe Islands', 'Aruba', 'Isle of Man,Congo',
             'Timor-Leste', 'Taiwan', 'Cayman Islands', 'Gibraltar', 'CAR', 'Bermuda', 'Gambia', 'Greenland',
             'Saint Martin', 'Sint Maarten', 'Caribbean Netherlands','St. Vincent Grenadines', 'British Virgin Islands',
             'Turks and Caicos', 'St. Barth', 'Anguilla', 'Saint Pierre Miquelon', 'Cook Islands',
             'Wallis and Futuna', 'Montserrat', 'Falkland Islands', 'Macao', 'Niue']
        idx = 1
        for i in name_differences_country_list:
            if idx < 5:
                print(i, end=" - ")
                idx += 1
            else:
                print("\n")
                idx = 1
        print("\n")

        test = self.__my_sql_obj.exist_country_ck()
        return cleaning_cursor(test)
